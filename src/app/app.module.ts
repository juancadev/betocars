import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { ClientesComponent } from './views/clientes/clientes.component';
import { VehiculosComponent } from './views/vehiculos/vehiculos.component';
import { ProductosComponent } from './views/productos/productos.component';
import { FacturacionComponent } from './views/facturacion/facturacion.component';
import { AgregarClienteComponent } from './views/clientes/agregar-cliente/agregar-cliente.component';
import { AgregarVehiculoComponent } from './views/vehiculos/agregar-vehiculo/agregar-vehiculo.component';
import { AgregarProductoComponent } from './views/productos/agregar-producto/agregar-producto.component';
import { AgregarFacturaComponent } from './views/facturacion/agregar-factura/agregar-factura.component';

import { FormsModule } from '@angular/forms';
import { FilterClientePipe } from './pipes/filter-cliente.pipe';
import { FilterProductoPipe } from './pipes/filter-producto.pipe';
import { FilterVehiculoPipe } from './pipes/filter-vehiculo.pipe';
import { HomeComponent } from './views/home/home.component';
import { FacturaCreditoComponent } from './views/facturacion/factura-credito/factura-credito.component';

import localEsCo from '@angular/common/locales/es-CO';
import { registerLocaleData } from '@angular/common';
import { DetalleFacturaComponent } from './views/facturacion/detalle-factura/detalle-factura.component';
import { FilterFacturaPipe } from './pipes/filter-factura.pipe';
import { SalidaComponent } from './views/facturacion/salida/salida.component';
import { TruncatePipe } from './pipes/truncate.pipe';
import { FiltrarFacturaFechaPipe } from './pipes/filtrar-factura-fecha.pipe';
import { FiltrarClienteNombrePipe } from './pipes/filtrar-cliente-nombre.pipe';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FilterSalidaPipe } from './pipes/filter-salida.pipe';

registerLocaleData(localEsCo, 'es-CO');

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ClientesComponent,
    VehiculosComponent,
    ProductosComponent,
    FacturacionComponent,
    AgregarClienteComponent,
    AgregarVehiculoComponent,
    AgregarProductoComponent,
    AgregarFacturaComponent,
    FilterClientePipe,
    FilterProductoPipe,
    FilterVehiculoPipe,
    HomeComponent,
    FacturaCreditoComponent,
    DetalleFacturaComponent,
    FilterFacturaPipe,
    SalidaComponent,
    TruncatePipe,
    FiltrarFacturaFechaPipe,
    FiltrarClienteNombrePipe,
    FilterSalidaPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-CO' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
