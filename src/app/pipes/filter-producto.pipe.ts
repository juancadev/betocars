import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterProducto'
})
export class FilterProductoPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultProductos = [];
    for (const producto of value) {
      if (producto.codigo.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultProductos.push(producto);
      }
    }
    return resultProductos;
  }

}
