import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterVehiculo'
})
export class FilterVehiculoPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultVehiculos = [];
    for (const vehiculo of value) {
      if (vehiculo.placa_vh.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultVehiculos.push(vehiculo);
      }
    }
    return resultVehiculos;
  }

}
