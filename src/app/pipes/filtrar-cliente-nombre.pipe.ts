import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtrarClienteNombre'
})
export class FiltrarClienteNombrePipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultClientes = [];
    for (const cliente of value) {
      const nombreCompleto = `${cliente.nombres_cliente} ${cliente.apellidos_cliente}`;
      if (nombreCompleto.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultClientes.push(cliente);
      }
    }
    return resultClientes;
  }

}
