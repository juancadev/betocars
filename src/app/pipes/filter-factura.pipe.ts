import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterFactura'
})
export class FilterFacturaPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultServicios = [];
    for (const servicio of value) {
      if (servicio.placa_vh.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultServicios.push(servicio);
      }
    }
    return resultServicios;
  }

}
