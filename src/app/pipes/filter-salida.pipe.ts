import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSalida'
})
export class FilterSalidaPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultSalidas = [];
    for (const salida of value) {
      if (salida.motivo_sal.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultSalidas.push(salida);
      }
    }
    return resultSalidas;
  }

}
