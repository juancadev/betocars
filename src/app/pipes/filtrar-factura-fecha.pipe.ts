import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterFacturaFecha'
})
export class FiltrarFacturaFechaPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultServicios = [];
    for (const servicio of value) {
      if (servicio.fecha_letras_svcio.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultServicios.push(servicio);
      }
    }
    return resultServicios;
  }

}
