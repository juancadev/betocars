import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientesComponent } from './views/clientes/clientes.component';
import { VehiculosComponent } from './views/vehiculos/vehiculos.component';
import { ProductosComponent } from './views/productos/productos.component';
import { FacturacionComponent } from './views/facturacion/facturacion.component';
import { AgregarClienteComponent } from './views/clientes/agregar-cliente/agregar-cliente.component';
import { AgregarVehiculoComponent } from './views/vehiculos/agregar-vehiculo/agregar-vehiculo.component';
import { AgregarProductoComponent } from './views/productos/agregar-producto/agregar-producto.component';
import { AgregarFacturaComponent } from './views/facturacion/agregar-factura/agregar-factura.component';
import { HomeComponent } from './views/home/home.component';
import { FacturaCreditoComponent } from './views/facturacion/factura-credito/factura-credito.component';
import { DetalleFacturaComponent } from './views/facturacion/detalle-factura/detalle-factura.component';
import { SalidaComponent } from './views/facturacion/salida/salida.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'clientes', component: ClientesComponent },
  { path: 'clientes/agregar', component: AgregarClienteComponent },
  { path: 'clientes/editar/:id', component: AgregarClienteComponent },
  { path: 'vehiculos', component: VehiculosComponent },
  { path: 'vehiculos/agregar', component: AgregarVehiculoComponent },
  { path: 'vehiculos/editar/:id', component: AgregarVehiculoComponent },
  { path: 'productos', component: ProductosComponent },
  { path: 'productos/agregar', component: AgregarProductoComponent },
  { path: 'productos/editar/:id', component: AgregarProductoComponent },
  { path: 'facturacion', component: FacturacionComponent },
  { path: 'facturacion/agregar', component: AgregarFacturaComponent },
  { path: 'facturacion/agregar/:id', component: AgregarFacturaComponent },
  { path: 'facturacion/credito', component: FacturaCreditoComponent },
  { path: 'facturacion/salida', component: SalidaComponent },
  { path: 'facturacion/detalle/:id', component: DetalleFacturaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
