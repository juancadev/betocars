import { Component, OnInit, DoCheck } from '@angular/core';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { Vehiculo } from 'src/models/vehiculo';
import { ClienteService } from 'src/app/services/cliente.service';
import { Cliente } from 'src/models/cliente';
import { TipoVehiculo } from 'src/models/tipoVehiculo';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-agregar-vehiculo',
  templateUrl: './agregar-vehiculo.component.html',
  styleUrls: ['./agregar-vehiculo.component.scss']
})
export class AgregarVehiculoComponent implements OnInit {
  tipoVehiculo = TipoVehiculo;
  vehiculo: Vehiculo = {
    cliente_idCliente: null,
    tipoVehiculos: null,
    placa_vh: null,
    marca_vh: null,
    modelo_vh: null,
    // filtro_aceite: null,
    // filtro_aire: null,
    // filtro_cabina: null,
    // filtro_combustible: null,
    // caja: null,
    // transmision: null,
    otros_vh: null
  };
  clientes: Cliente[];
  cliente: Cliente;
  nombreCompleto: string;

  titulo: string;

  edit = false;

  constructor(
    private vehiculoService: VehiculoService,
    private clienteService: ClienteService,
    private route: ActivatedRoute,
    private router: Router
    ) {}

  ngOnInit() {
    this.getClientes();
    this.route.params.subscribe(params => {
      console.log('Parametros', params);
      // Crear vehiculo
      if (Object.keys(params).length === 0) {
        this.edit = false;
        this.titulo = 'Registro de vehiculos';
        console.log('Vehiculo', this.vehiculo);
      } else {
        // Editar vehiculo
        this.edit = true;
        this.titulo = 'Editar vehiculo';
        this.vehiculoService.getVehiculoById(params.id).subscribe(
          (data: any) => {
            this.vehiculo = data.vehiculo;
            // Cambiar el vehiculo.cliente_idCliente por la cedula
            this.getClienteById(this.vehiculo.cliente_idCliente);
            console.log('Vehiculo', this.vehiculo);
          },
          error => console.error('Error: ' + error)
        );
      }
    });
  }

  createVehiculo() {
    // Cambio la cc por el id en vehiculo.cliente_idCliente
    const cedula = this.vehiculo.cliente_idCliente;
    this.clienteService.getClienteByCc(this.vehiculo.cliente_idCliente).subscribe(
      (data: any) => {
        this.vehiculo.cliente_idCliente = data.cliente._id;
        // Agrego a la db
        console.log(this.vehiculo);
        this.vehiculoService.createVehiculo(this.vehiculo).subscribe(
          success => {
            alert('Vehiculo creado correctamente');
            this.goBack();
          },
          error => {
            alert('Error al crear: Verifique los campos obligatorios');
            // Modificar de nuevo el id por cedula
            this.vehiculo.cliente_idCliente = cedula;
          }
        );
      },
      error => alert('Error al traer cliente: Verifique el campo cliente')
    );
  }

  updateVehiculo() {
    // Cambio la cc por el id en vehiculo.cliente_idCliente
    this.clienteService.getClienteByCc(this.vehiculo.cliente_idCliente).subscribe(
      (data: any) => {
        this.vehiculo.cliente_idCliente = data.cliente._id;
        // Agrego a la db
        console.log(this.vehiculo);
        this.vehiculoService.updateVehiculo(this.vehiculo._id, this.vehiculo).subscribe(
          success => {
            alert('Vehiculo modificado correctamente');
            this.goBack();
        },
          error => alert('Error al modificar')
        );
      },
      error => console.log('Error al traer cliente por cc')
    );
  }

  getClientes() {
    this.clienteService.getClientes().subscribe(
      (data: any) => this.clientes = data.clientes,
      error => alert('Aún no hay clientes: ' + error)
    );
  }

  getClienteByCc(cc: string) {
    this.clienteService.getClienteByCc(cc).subscribe(
      (data: any) => {
        this.cliente = data.cliente;
        this.nombreCompleto = `${this.cliente.nombres_cliente} ${this.cliente.apellidos_cliente}`;
      },
      error => console.log('Error al traer cliente por cc')
    );
  }

  getClienteById(id: string) {
    this.clienteService.getClienteById(id).subscribe(
      (data: any) => this.vehiculo.cliente_idCliente = data.cliente.cc_cliente,
      error => console.log('Error al traer el cliente')
    );
  }

  goBack() {
    this.router.navigate(['vehiculos']);
  }

}
