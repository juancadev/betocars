import { Component, OnInit } from '@angular/core';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { Vehiculo } from 'src/models/vehiculo';
import { ClienteService } from 'src/app/services/cliente.service';
import { Cliente } from 'src/models/cliente';

@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.scss']
})
export class VehiculosComponent implements OnInit {
  vehiculos: Vehiculo[];
  vehiculo: Vehiculo;
  cliente: Cliente;

  filter = '';

  constructor(private vehiculosService: VehiculoService, private clientesService: ClienteService) { }

  ngOnInit() {
    this.getVehiculos();
  }

  getVehiculos() {
    this.vehiculosService.getVehiculos().subscribe(
      (data: any) => {
        this.vehiculos = data.vehiculos;
        console.log('Vehiculos', this.vehiculos);
      },
      error => console.log('Error al traer clientes: ' + error)
    );
  }

  deleteVehiculo(id: string) {
    const eliminar = confirm('¿Estás seguro de eliminar?');
    if (eliminar) {
      this.vehiculosService.deleteVehiculo(id).subscribe(
        () => {
          this.getVehiculos();
          alert('Vehiculo eliminado correctamente');
        },
        () => alert('Error al eliminar')
      );
    }
  }

  getVehiculo(vehiculo: Vehiculo) {
    this.vehiculo = vehiculo;
    console.log(this.vehiculo);
    this.getClienteById(this.vehiculo.cliente_idCliente);
  }

  getClienteById(id: string) {
    this.clientesService.getClienteById(id).subscribe(
      (data: any) => this.cliente = data.cliente,
      error => console.log('Error al traer cliente: ' + error)
    );
  }

}
