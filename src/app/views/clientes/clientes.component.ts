import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { Cliente } from 'src/models/cliente';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { Vehiculo } from 'src/models/vehiculo';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {
  clientes: Cliente[];
  cliente: Cliente;
  vehiculos: Vehiculo[];

  filter = '';
  typeFilter = 'cc';

  constructor(
    private clientesService: ClienteService,
    private vehiculoService: VehiculoService
  ) {}

  ngOnInit() {
    this.getClientes();
  }

  getClientes() {
    this.clientesService.getClientes().subscribe(
      (data: any) => {
        this.clientes = data.clientes;
        console.log('Clientes', this.clientes);
      },
      error => console.log('Error al traer clientes: ' + error)
    );
  }
  deleteCliente(id: string) {
    const eliminar = confirm('¿Estás seguro de eliminar?');
    if (eliminar) {
      this.clientesService.deleteCliente(id).subscribe(
        () => {
          this.getClientes();
          alert('Cliente eliminado correctamente');
        },
        () => alert('Error al eliminar')
      );
    }
  }

  getCliente(cliente: Cliente) {
    this.cliente = cliente;
    this.getVehiculosByClienteCc(this.cliente.cc_cliente);
    console.log(this.cliente);
  }

  getVehiculosByClienteCc(cc: string) {
    this.vehiculoService.getVehiculosByClienteCc(cc).subscribe(
      (data: any) => this.vehiculos = data.vehiculo,
      error => console.log('Error al traer vehiculos')
    );
  }

}
