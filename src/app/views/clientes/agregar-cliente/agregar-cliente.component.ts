import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteService } from 'src/app/services/cliente.service';
import { Cliente } from 'src/models/cliente';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-agregar-cliente',
  templateUrl: './agregar-cliente.component.html',
  styleUrls: ['./agregar-cliente.component.scss']
})
export class AgregarClienteComponent implements OnInit {
  titulo: string;
  clientes: Cliente[];
  cliente: Cliente = {
    nombres_cliente: null,
    apellidos_cliente: null,
    cc_cliente: null,
    cel_cliente: null,
    correo_cliente: null,
    direccion_cliente: null
  };
  edit = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private clientesService: ClienteService
    ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log('Parametros', params);
      // Crear cliente
      if (Object.keys(params).length === 0) {
        this.edit = false;
        this.titulo = 'Registro de clientes';
        console.log('Cliente', this.cliente);
      } else {
        // Editar cliente
        this.edit = true;
        this.titulo = 'Editar cliente';
        this.clientesService.getClienteById(params.id).subscribe(
          (data: any) => {
            this.cliente = data.cliente;
            console.log('Cliente', this.cliente);
          },
          error => console.error('Error: ' + error)
        );
      }
    });
  }

  createCliente() {
    this.clientesService.createCliente(this.cliente).subscribe(
      success => {
        alert('Cliente creado con éxito');
        this.goBack();
      },
      error => alert('Error al crear: Verifique los campos obligatorios')
    );
  }

  updateCliente() {
    this.clientesService.updateCliente(this.cliente._id, this.cliente).subscribe(
      success => {
        alert('Cliente modificado con éxito');
        this.goBack();
      },
      error => alert('Error al modificar: Verifique los campos obligatorios')
    );
  }

  goBack() {
    this.router.navigate(['clientes']);
  }

}
