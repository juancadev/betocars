import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/models/producto';
import { ProductoService } from 'src/app/services/producto.service';
import { TipoProducto } from 'src/models/tipoProducto';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-agregar-producto',
  templateUrl: './agregar-producto.component.html',
  styleUrls: ['./agregar-producto.component.scss']
})
export class AgregarProductoComponent implements OnInit {
  tipoProducto = TipoProducto;
  producto: Producto = {
    tipo_producto: null,
    codigo: null,
    referencia: null,
    cantidad_actual: null,
    precio_compra: null,
    precio_venta: null,
    notas: null,
    tipo_vh_compatible: null,
    equivalencia: null
  };
  edit = false;
  titulo: string;

  constructor(
    private productoService: ProductoService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    console.log(this.tipoProducto);
    this.route.params.subscribe(params => {
      console.log('Parametros', params);
      // Crear producto
      if (Object.keys(params).length === 0) {
        this.edit = false;
        this.titulo = 'Registro de productos';
        console.log('Producto', this.producto);
      } else {
        // Editar producto
        this.edit = true;
        this.titulo = 'Editar producto';
        this.productoService.getProductoById(params.id).subscribe(
          (data: any) => {
            this.producto = data.producto;
            console.log('Producto', this.producto);
          },
          error => console.error('Error')
        );
      }
    });
  }

  createProducto() {
    this.productoService.createProducto(this.producto).subscribe(
      success => {
        alert('Producto creado correctamente');
        this.goBack();
      },
      error => alert('Error al crear')
    );
  }

  updateProducto() {
    this.productoService.updateProducto(this.producto._id, this.producto).subscribe(
      success => {
        alert('Producto modificado correctamente');
        this.goBack();
      },
      error => alert('Error al modificar')
    );
  }

  goBack() {
    this.router.navigate(['productos']);
  }

}
