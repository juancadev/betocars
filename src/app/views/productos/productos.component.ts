import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/services/producto.service';
import { Producto } from 'src/models/producto';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {
  productos: Producto[];
  producto: Producto;
  closeResult: string;

  filter = '';

  // Producto existente
  codigoExistente: string;
  cantidadNueva: number;

  constructor(
    private productoService: ProductoService
  ) { }

  ngOnInit() {
    this.getProductos();
  }

  getProductos() {
    this.productoService.getProductos().subscribe(
      (data: any) => {
        this.productos = data.productos;
        console.log('Productos', this.productos);
      },
      error => console.log('Error al traer los productos: ' + error)
    );
  }

  deleteProducto(id: string) {
    const eliminar = confirm('¿Estás seguro de eliminar?');
    if (eliminar) {
      this.productoService.deleteProducto(id).subscribe(
        () => {
          console.log('Eliminado', id);
          this.getProductos();
        },
        () => console.log('Error al eliminar')
      );
    }
  }

  getProducto(producto: Producto) {
    this.producto = producto;
    console.log(this.producto);
  }

  imprimir(div: any) {
    const contenido = document.getElementById(div).innerHTML;
    const contenidoOriginal = document.body.innerHTML;
    document.body.innerHTML = contenido;

    window.print();

    document.body.innerHTML = contenidoOriginal;
    location.reload();
  }

  // Producto exitente
  updateProducto() {
    this.productoService.getProductoByCodigo(this.codigoExistente).subscribe(
      (data: any) => {
        const productoNuevoExistente = {
          codigo: data.producto.codigo,
          cantidad_actual: (data.producto.cantidad_actual + this.cantidadNueva)
        };
        // Actualizar producto
        this.productoService.updateProducto(data.producto._id, productoNuevoExistente).subscribe(
          () => {
            alert('Producto actualizado correctamente');
            this.getProductos();
          },
          error => alert('Error al actualizar el producto')
        );
      },
      error => alert('Error al traer producto')
    );
  }

}
