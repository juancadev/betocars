import { Component, OnInit } from '@angular/core';
import { Salida } from 'src/models/salida';
import { ServicioService } from 'src/app/services/servicio.service';

@Component({
  selector: 'app-salida',
  templateUrl: './salida.component.html',
  styleUrls: ['./salida.component.scss']
})
export class SalidaComponent implements OnInit {
  salida: Salida = {
    valor_sal: null,
    motivo_sal: null
  };
  salidas: Salida[];
  salidaById: Salida;

  editar: boolean;

  filter = '';

  constructor(private servicioService: ServicioService) { }

  ngOnInit() {
    this.getSalidas();
  }

  getSalidas() {
    this.servicioService.getSalidas().subscribe(
      (data: any) => {
        this.salidas = data.Salidas;
        console.log('Salidas', this.salidas);
      },
      error => console.log('Error al traer salidas')
    );
  }

  getSalida(s: Salida) {
    this.editar = false;
    this.salida = {
      _id: s._id,
      valor_sal: s.valor_sal,
      motivo_sal: s.motivo_sal
    };
  }

  cleanSalida() {
    this.editar = true;
    this.salida = {
      valor_sal: null,
      motivo_sal: null
    };
  }

  createSalida() {
    this.servicioService.createSalida(this.salida).subscribe(
      () => {
        alert('Salida creada correctamente');
        this.getSalidas();
      },
      error => console.log('Error al crear salida')
    );
  }

  updateSalida() {
    this.servicioService.updateSalida(this.salida._id, this.salida).subscribe(
      () => {
        alert('Salida modificada correctamente');
        this.getSalidas();
      },
      error => console.log('Error al modificar salida')
    );
  }

  getSalidaById(id: string) {
    this.servicioService.getSalidaById(id).subscribe(
      (data: any) => this.salidaById = data.Salida,
      error => console.log('Error al traer salida by id')
    );
  }

  deleteSalida(id: string) {
    const eliminar = confirm('¿Estás seguro de eliminar?');
    if (eliminar) {
      this.servicioService.deleteSalida(id).subscribe(
        success => {
          this.getSalidas();
          alert('Salida eliminado correctamente');
        },
        error => alert('Error al eliminar salida')
      );
    }
  }

}
