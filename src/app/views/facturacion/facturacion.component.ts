import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { Servicio } from 'src/models/servicio';
import { ClienteService } from 'src/app/services/cliente.service';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { Cliente } from 'src/models/cliente';
import { Vehiculo } from 'src/models/vehiculo';
import { ServicioCompleto } from 'src/models/servicioCompleto';

@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.component.html',
  styleUrls: ['./facturacion.component.scss']
})
export class FacturacionComponent implements OnInit {
  servicios: Servicio[];
  serviciosCompletos: ServicioCompleto[];
  cliente: Cliente;
  vehiculo: Vehiculo;
  servicio: Servicio;

  filter = '';
  typeFilter = 'placa';

  constructor(
    private servicioService: ServicioService,
    private clienteService: ClienteService,
  ) { }

  ngOnInit() {
    this.getServicios();
    this.getServiciosCompletos();
  }

  getServicios() {
    this.servicioService.getServicios().subscribe(
      (data: any) => {
        this.servicios = data.servicios;
        console.log('Servicio', this.servicios);
      },
      error => console.log('Error al traer servicios')
    );
  }

  getServiciosCompletos() {
    this.servicioService.getServiciosCompleto().subscribe(
      (data: any) => {
        this.serviciosCompletos = data.serviciosPlacaCliente;
        console.log('Servicio completo', this.serviciosCompletos);
      }
    );
  }

  deleteServicio(id: string) {
    const eliminar = confirm('¿Estás seguro de eliminar?');
    if (eliminar) {
      this.servicioService.deleteServicio(id).subscribe(
        success => {
          this.getServiciosCompletos();
          alert('Servicio eliminado correctamente');
        },
        error => alert('Error al eliminar servicio')
      );
    }
  }

  getClienteById(id: string) {
    this.clienteService.getClienteById(id).subscribe(
      (data: any) => this.cliente = data.cliente,
      () => console.log('Error al traer cliente')
    );
  }

  getServicio(servicio: Servicio) {
    this.servicio = servicio;
    console.log(this.servicio);
  }

}
