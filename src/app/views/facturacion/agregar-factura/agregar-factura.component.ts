import { Component, OnInit } from '@angular/core';
import { Vehiculo } from 'src/models/vehiculo';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { Cliente } from 'src/models/cliente';
import { ProductoService } from 'src/app/services/producto.service';
import { Producto } from 'src/models/producto';

import { Servicio } from 'src/models/servicio';
import { TipoPagoServicio } from 'src/models/tipoPagoServicio';
import { ServicioService } from 'src/app/services/servicio.service';
import { ClienteService } from 'src/app/services/cliente.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-agregar-factura',
  templateUrl: './agregar-factura.component.html',
  styleUrls: ['./agregar-factura.component.scss']
})
export class AgregarFacturaComponent implements OnInit {
  fecha: string;

  clientes: Cliente[];
  cliente: Cliente;
  vehiculos: Vehiculo[];
  vehiculo: Vehiculo;
  servicioCompletoByIdCliente: any;

  ccCliente: string;

  // Para llenar los datalist de los input de cada producto
  productos: Producto[];
  productosTipoAceite = [];
  productosTipoFiltroAceite = [];
  productosTipoFiltroAire = [];
  productosTipoFiltroCabina = [];
  productosTipoFiltroCombustible = [];
  productosTipoVitrina = [];
  productosTipoAditivo = [];

  aceite1: string;
  aceite2: string;
  aceite3: string;
  aceite4: string;
  filtroAceite1: string;
  filtroAceite2: string;
  filtroAire1: string;
  filtroAire2: string;
  filtroCabina: string;
  filtroCombustible1: string;
  filtroCombustible2: string;
  filtroCombustible3: string;
  cajaValvulinas1: string;
  cajaValvulinas2: string;
  transmisionValvulinas1: string;
  transmisionValvulinas2: string;
  vitrina1: string;
  vitrina2: string;
  vitrina3: string;
  vitrina4: string;
  aditivo1: string;
  aditivo2: string;
  aditivo3: string;
  aditivo4: string;
  varios1: string;
  varios2: string;
  varios3: string;
  varios4: string;
  varios5: string;

  // Habilitar input de adelanto de pago
  opcionPago0: string = TipoPagoServicio.contado;
  opcionPago1: string = TipoPagoServicio.credito;

  nombreCliente = '';

  servicio: Servicio = {
    nro_factura_svcio: null,
    vehiculo_idVehiculo: null,
    cliente_idCliente: null,
    // nombre_cliente: null,
    placa_vehiculo: null,
    // Productos
    productos_array: [
    ],
    aceite_km_actual: null,
    aceite_km_proximo: null,
    caja_km_actual: null,
    caja_km_proximo: null,
    transmision_km_actual: null,
    transmision_km_proximo: null,
    descripcion_mano_obra_svcio: null,
    valor_mano_obra_svcio: null,
    // Pago
    estado_pago_svcio: null,
    total_precio_servicio: null,
    total_precio_condescuento: null,
    valor_cancelado_svcio: 0,

    otros_svcio: null
  };

  cantidadProductos = {
    aceite1: 0,
    aceite2: 0,
    aceite3: 0,
    aceite4: 0,
    filtroAceite1: 0,
    filtroAceite2: 0,
    filtroAire1: 0,
    filtroAire2: 0,
    filtroCabina: 0,
    filtroCombustible1: 0,
    filtroCombustible2: 0,
    filtroCombustible3: 0,
    cajaValvulinas1: 0,
    cajaValvulinas2: 0,
    transmisionValvulinas1: 0,
    transmisionValvulinas2: 0,
    vitrina1: 0,
    vitrina2: 0,
    vitrina3: 0,
    vitrina4: 0,
    aditivo1: 0,
    aditivo2: 0,
    aditivo3: 0,
    aditivo4: 0,
    varios1: 0,
    varios2: 0,
    varios3: 0,
    varios4: 0,
    varios5: 0
  };

  seleccionado = {
    aceite1: { _id: '', aceite1: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
    aceite2: { _id: '', aceite2: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
    aceite3: { _id: '', aceite3: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
    aceite4: { _id: '', aceite4: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    filtroAceite1: { _id: '', filtro_aceite1: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    filtroAceite2: { _id: '', filtro_aceite2: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    filtroAire1: {_id: '', filtro_aire1: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    filtroAire2: {_id: '', filtro_aire2: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    filtroCabina: { _id: '', filtro_cabina: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    filtroCombustible1: { _id: '', filtro_combustible1: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line:max-line-length
    filtroCombustible2: { _id: '', filtro_combustible2: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    filtroCombustible3: { _id: '', filtro_combustible3: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    cajaValvulinas1: { _id: '', caja1: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line:max-line-length
    cajaValvulinas2: { _id: '', caja2: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    transmisionValvulinas1: { _id: '', transmision1: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    transmisionValvulinas2: { _id: '', transmision2: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    vitrina1: { _id: '', vitrina1: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    vitrina2: { _id: '', vitrina2: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    vitrina3: { _id: '', vitrina3: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    vitrina4: { _id: '', vitrina4: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    aditivo1: { _id: '', aditivo1: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    aditivo2: { _id: '', aditivo2: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    aditivo3: { _id: '', aditivo3: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
// tslint:disable-next-line: max-line-length
    aditivo4: { _id: '', aditivo4: '', codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
    varios1: { _id: '',  codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
    varios2: { _id: '',  codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
    varios3: { _id: '',  codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
    varios4: { _id: '',  codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 },
    varios5: { _id: '',  codigo: '', tipo_producto: '', cantidad_actual: 0, referencia: '', precio_unidad: 0, precio_venta: 0 }
  };

  tieneVehiculo = 'si';

  constructor(
    private vehiculoService: VehiculoService,
    private productoService: ProductoService,
    private servicioService: ServicioService,
    private clienteService: ClienteService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    const f = new Date();
    this.fecha = `${f.getDate()}/${f.getMonth() + 1}/${f.getFullYear()}`;
  }

  ngOnInit() {
    this.getServicios();
    this.servicio.estado_pago_svcio = TipoPagoServicio.contado;
    console.log(this.servicio.estado_pago_svcio);
  }

  getServicios() {
    this.getVehiculos();
    this.getClientes();
    this.getParamsVehiculo();
    this.productoService.getProductos().subscribe(
      (data: any) => {
        this.productos = data.productos;
        this.getTipoProductos(this.productos);
      },
      error => console.log('Error al traer productos')
      );
    console.log(this.servicio);
  }

  createServicio() {
    console.log('Servicio', this.servicio);
    // Tipo servicio 'contado'
    if (this.servicio.estado_pago_svcio === TipoPagoServicio.contado) {
      this.servicio.valor_cancelado_svcio = this.servicio.total_precio_condescuento;
      console.log(this.servicio.valor_cancelado_svcio);
      console.log(this.servicio.total_precio_condescuento);
    }
    // Guardar productos en array
    this.guardarProductos();
    // Cambiar cédula por id SI NO tiene vehiculo
    console.log('Servicio', this.servicio);
    if (this.tieneVehiculo === 'no') {
      this.clienteService.getClienteByCc(this.servicio.cliente_idCliente).subscribe(
        (data: any) => {
          console.log(data.cliente._id);
          this.ccCliente = this.servicio.cliente_idCliente;
          this.servicio.cliente_idCliente = data.cliente._id;
          // Agrego a la db
          // Crear servicio
          this.servicioService.createServicio(this.servicio).subscribe(
            () => {
              // Actualizar cantidad productos
              this.servicio.productos_array.forEach((producto: any) => {
                this.updateProductoCantidad(producto._id, producto);
              });
              alert('Servicio creado corretamente');
              console.log('Servicio creado: ', this.servicio);
              this.goBack();
            },
            () => {
              alert('Error al crear servicio: Verifica todos los campos obligatorios');
              // Vaciamos productos y volvemos a colocar cc cliente
              this.servicio.productos_array = [];
              this.servicio.cliente_idCliente = this.ccCliente;
            }
          );
        },
        error => console.log('Error al traer cliente por cc')
      );
    } else {
      // Crear servicio
      console.log(this.servicio.total_precio_condescuento);
      this.servicioService.createServicio(this.servicio).subscribe(
        () => {
          // Actualizar cantidad productos
          console.log(this.servicio.total_precio_condescuento);
          this.servicio.productos_array.forEach((producto: any) => {
            this.updateProductoCantidad(producto._id, producto);
          });
          alert('Servicio creado correctamente');
          console.log(this.servicio.total_precio_condescuento);
          this.goBack();
        },
        () => {
          alert('Error al crear servicio: Verifica los campos obligatorios');
          // Vaciamos productos y volvemos a colocar cc cliente
          this.servicio.productos_array = [];
          console.log('Servicio', this.servicio);
        }
      );
    }
  }

  getClienteByPlacaVehiculo(placa: string) {
    this.vehiculoService.getClienteByPlacaVehiculo(placa).subscribe(
      (data: any) => {
        this.servicio.vehiculo_idVehiculo = data.vehiculo._id,
        this.servicio.placa_vehiculo = data.vehiculo.placa_vh;
        this.servicio.cliente_idCliente = data.cliente._id;
        console.log(this.servicio);
        this.cliente = data.cliente;
        this.nombreCliente = `${data.cliente.nombres_cliente} ${data.cliente.apellidos_cliente}`;
        this.servicio.nombre_cliente = this.nombreCliente;

        this.getServicioCompletoByIdCliente(this.servicio.cliente_idCliente);
      },
      error => {
        this.nombreCliente = '';
      }
    );
  }

  getNombreClienteByCc(cc: string) {
    this.clienteService.getClienteByCc(cc).subscribe(
      (data: any) => this.nombreCliente = `${data.cliente.nombres_cliente} ${data.cliente.apellidos_cliente}`,
      error => {
        console.log('Error al traer nombre cliente by cc');
        this.nombreCliente = '';
      }
    );
  }

  getVehiculos() {
    this.vehiculoService.getVehiculos().subscribe(
      (data: any) => this.vehiculos = data.vehiculos,
      error => console.log('Error al traer los vehiculos: ' + error)
    );
  }

  getClientes() {
    this.clienteService.getClientes().subscribe(
      (data: any) => this.clientes = data.clientes,
      error => console.log('Error al traer los clientes: ' + error)
    );
  }

  getServicioCompletoByIdCliente(id: string) {
    this.servicioService.getServicioCompletoByIdCliente(id).subscribe(
      (data: any) => {
        this.servicioCompletoByIdCliente = data.serviciosCompletos;
        console.log(this.servicioCompletoByIdCliente);
      },
      error => console.log('Error al tarer servicioCompletoByCliente')
    );
  }

  getTipoProductos(productos: any) {
    productos.forEach((producto: any) => {
      switch (producto.tipo_producto) {
        case 'Aceite': this.productosTipoAceite.push(producto); break;
        case 'Filtro de aceite': this.productosTipoFiltroAceite.push(producto); break;
        case 'Filtro de aire': this.productosTipoFiltroAire.push(producto); break;
        case 'Filtro de cabina': this.productosTipoFiltroCabina.push(producto); break;
        case 'Filtro de combustible': this.productosTipoFiltroCombustible.push(producto); break;
        case 'Vitrina': this.productosTipoVitrina.push(producto); break;
        case 'Aditivo': this.productosTipoAditivo.push(producto); break;
      }
    });
  }

  guardarProductos() {
    const precioFinalAceite1 = this.seleccionado.aceite1.precio_venta * this.cantidadProductos.aceite1;
    const precioFinalAceite2 = this.seleccionado.aceite2.precio_venta * this.cantidadProductos.aceite2;
    const precioFinalAceite3 = this.seleccionado.aceite3.precio_venta * this.cantidadProductos.aceite3;
    const precioFinalAceite4 = this.seleccionado.aceite4.precio_venta * this.cantidadProductos.aceite4;
    const precioFinalFiltroAceite1 = this.seleccionado.filtroAceite1.precio_venta * this.cantidadProductos.filtroAceite1;
    const precioFinalFiltroAceite2 = this.seleccionado.filtroAceite2.precio_venta * this.cantidadProductos.filtroAceite2;
    const precioFinalFiltroAire1 = this.seleccionado.filtroAire1.precio_venta * this.cantidadProductos.filtroAire1;
    const precioFinalFiltroAire2 = this.seleccionado.filtroAire2.precio_venta * this.cantidadProductos.filtroAire2;
    const precioFinalFiltroCabina = this.seleccionado.filtroCabina.precio_venta * this.cantidadProductos.filtroCabina;
    const precioFinalFiltroCombustible1 = this.seleccionado.filtroCombustible1.precio_venta * this.cantidadProductos.filtroCombustible1;
    const precioFinalFiltroCombustible2 = this.seleccionado.filtroCombustible2.precio_venta * this.cantidadProductos.filtroCombustible2;
    const precioFinalFiltroCombustible3 = this.seleccionado.filtroCombustible3.precio_venta * this.cantidadProductos.filtroCombustible3;
    const precioFinalCajaValvulinas1 = this.seleccionado.cajaValvulinas1.precio_venta * this.cantidadProductos.cajaValvulinas1;
    const precioFinalCajaValvulinas2 = this.seleccionado.cajaValvulinas2.precio_venta * this.cantidadProductos.cajaValvulinas2;
// tslint:disable-next-line:max-line-length
    const precioFinalTransmisionValvulinas1 = this.seleccionado.transmisionValvulinas1.precio_venta * this.cantidadProductos.transmisionValvulinas1;
// tslint:disable-next-line: max-line-length
    const precioFinalTransmisionValvulinas2 = this.seleccionado.transmisionValvulinas2.precio_venta * this.cantidadProductos.transmisionValvulinas2;
    const precioFinalVitrina1 = this.seleccionado.vitrina1.precio_venta * this.cantidadProductos.vitrina1;
    const precioFinalVitrina2 = this.seleccionado.vitrina2.precio_venta * this.cantidadProductos.vitrina2;
    const precioFinalVitrina3 = this.seleccionado.vitrina3.precio_venta * this.cantidadProductos.vitrina3;
    const precioFinalVitrina4 = this.seleccionado.vitrina4.precio_venta * this.cantidadProductos.vitrina4;
    const precioFinalAditivo1 = this.seleccionado.aditivo1.precio_venta * this.cantidadProductos.aditivo1;
    const precioFinalAditivo2 = this.seleccionado.aditivo2.precio_venta * this.cantidadProductos.aditivo2;
    const precioFinalAditivo3 = this.seleccionado.aditivo3.precio_venta * this.cantidadProductos.aditivo3;
    const precioFinalAditivo4 = this.seleccionado.aditivo4.precio_venta * this.cantidadProductos.aditivo4;
    const precioFinalVarios1 = this.seleccionado.varios1.precio_venta * this.cantidadProductos.varios1;
    const precioFinalVarios2 = this.seleccionado.varios2.precio_venta * this.cantidadProductos.varios2;
    const precioFinalVarios3 = this.seleccionado.varios3.precio_venta * this.cantidadProductos.varios3;
    const precioFinalVarios4 = this.seleccionado.varios4.precio_venta * this.cantidadProductos.varios4;
    const precioFinalVarios5 = this.seleccionado.varios5.precio_venta * this.cantidadProductos.varios5;
    if (precioFinalAceite1 > 0) {
      console.log('Precio venta aceite: ' + this.seleccionado.aceite1.precio_venta);
      console.log('Aceite seleccionado: ' + this.seleccionado.aceite1);
      const aceite = {
        _id: this.seleccionado.aceite1._id,
        aceite1: this.seleccionado.aceite1.referencia,
        codigo: this.seleccionado.aceite1.codigo,
        tipo_producto: this.seleccionado.aceite1.tipo_producto,
        cantidad_actual: this.cantidadProductos.aceite1,
        referencia: this.seleccionado.aceite1.referencia,
        precio_unidad: this.seleccionado.aceite1.precio_venta,
        precio_venta: (this.cantidadProductos.aceite1 * this.seleccionado.aceite1.precio_venta)
      };
      console.log('Agregando aceite: ' + aceite);
      this.servicio.productos_array.push(aceite);
    }
    if (precioFinalAceite2 > 0) {
      console.log('Precio venta aceite: ' + this.seleccionado.aceite2.precio_venta);
      console.log('Aceite seleccionado: ' + this.seleccionado.aceite2);
      const aceite = {
        _id: this.seleccionado.aceite2._id,
        aceite2: this.seleccionado.aceite2.referencia,
        codigo: this.seleccionado.aceite2.codigo,
        tipo_producto: this.seleccionado.aceite2.tipo_producto,
        cantidad_actual: this.cantidadProductos.aceite2,
        referencia: this.seleccionado.aceite2.referencia,
        precio_unidad: this.seleccionado.aceite2.precio_venta,
        precio_venta: (this.cantidadProductos.aceite2 * this.seleccionado.aceite2.precio_venta)
      };
      console.log('Agregando aceite: ' + aceite);
      this.servicio.productos_array.push(aceite);
    }
    if (precioFinalAceite3 > 0) {
      console.log('Precio venta aceite: ' + this.seleccionado.aceite3.precio_venta);
      console.log('Aceite seleccionado: ' + this.seleccionado.aceite3);
      const aceite = {
        _id: this.seleccionado.aceite3._id,
        aceite3: this.seleccionado.aceite3.referencia,
        codigo: this.seleccionado.aceite3.codigo,
        tipo_producto: this.seleccionado.aceite3.tipo_producto,
        cantidad_actual: this.cantidadProductos.aceite3,
        referencia: this.seleccionado.aceite3.referencia,
        precio_unidad: this.seleccionado.aceite3.precio_venta,
        precio_venta: (this.cantidadProductos.aceite3 * this.seleccionado.aceite3.precio_venta)
      };
      console.log('Agregando aceite: ' + aceite);
      this.servicio.productos_array.push(aceite);
    }
    if (precioFinalAceite4 > 0) {
      console.log('Precio venta aceite: ' + this.seleccionado.aceite4.precio_venta);
      console.log('Aceite seleccionado: ' + this.seleccionado.aceite4);
      const aceite = {
        _id: this.seleccionado.aceite4._id,
        aceite4: this.seleccionado.aceite4.referencia,
        codigo: this.seleccionado.aceite4.codigo,
        tipo_producto: this.seleccionado.aceite4.tipo_producto,
        cantidad_actual: this.cantidadProductos.aceite4,
        referencia: this.seleccionado.aceite4.referencia,
        precio_unidad: this.seleccionado.aceite4.precio_venta,
        precio_venta: (this.cantidadProductos.aceite4 * this.seleccionado.aceite4.precio_venta)
      };
      console.log('Agregando aceite: ' + aceite);
      this.servicio.productos_array.push(aceite);
    }
    if (precioFinalFiltroAceite1 > 0) {
      const filtroAceite1 = {
        _id: this.seleccionado.filtroAceite1._id,
        filtro_aceite1: this.seleccionado.filtroAceite1.referencia,
        codigo: this.seleccionado.filtroAceite1.codigo,
        tipo_producto: this.seleccionado.filtroAceite1.tipo_producto,
        cantidad_actual: this.cantidadProductos.filtroAceite1,
        referencia: this.seleccionado.filtroAceite1.referencia,
        precio_unidad: this.seleccionado.filtroAceite1.precio_venta,
        precio_venta: (this.cantidadProductos.filtroAceite1 * this.seleccionado.filtroAceite1.precio_venta)
      };
      this.servicio.productos_array.push(filtroAceite1);
    }
    if (precioFinalFiltroAceite2 > 0) {
      const filtroAceite2 = {
        _id: this.seleccionado.filtroAceite2._id,
        filtro_aceite2: this.seleccionado.filtroAceite2.referencia,
        codigo: this.seleccionado.filtroAceite2.codigo,
        tipo_producto: this.seleccionado.filtroAceite2.tipo_producto,
        cantidad_actual: this.cantidadProductos.filtroAceite2,
        referencia: this.seleccionado.filtroAceite2.referencia,
        precio_unidad: this.seleccionado.filtroAceite2.precio_venta,
        precio_venta: (this.cantidadProductos.filtroAceite2 * this.seleccionado.filtroAceite2.precio_venta)
      };
      this.servicio.productos_array.push(filtroAceite2);
    }
    if (precioFinalFiltroAire1 > 0) {
      const filtroAire1 = {
        _id: this.seleccionado.filtroAire1._id,
        filtro_aire1: this.seleccionado.filtroAire1.referencia,
        codigo: this.seleccionado.filtroAire1.codigo,
        tipo_producto: this.seleccionado.filtroAire1.tipo_producto,
        referencia: this.seleccionado.filtroAire1.referencia,
        cantidad_actual: this.cantidadProductos.filtroAire1,
        precio_unidad: this.seleccionado.filtroAire1.precio_venta,
        precio_venta: (this.cantidadProductos.filtroAire1 * this.seleccionado.filtroAire1.precio_venta)
      };
      this.servicio.productos_array.push(filtroAire1);
    }
    if (precioFinalFiltroAire2 > 0) {
      const filtroAire2 = {
        _id: this.seleccionado.filtroAire2._id,
        filtro_aire2: this.seleccionado.filtroAire2.referencia,
        codigo: this.seleccionado.filtroAire2.codigo,
        tipo_producto: this.seleccionado.filtroAire2.tipo_producto,
        referencia: this.seleccionado.filtroAire2.referencia,
        cantidad_actual: this.cantidadProductos.filtroAire2,
        precio_unidad: this.seleccionado.filtroAire2.precio_venta,
        precio_venta: (this.cantidadProductos.filtroAire2 * this.seleccionado.filtroAire2.precio_venta)
      };
      this.servicio.productos_array.push(filtroAire2);
    }
    if (precioFinalFiltroCabina > 0) {
      const filtroCabina = {
        _id: this.seleccionado.filtroCabina._id,
        filtro_cabina: this.seleccionado.filtroCabina.referencia,
        codigo: this.seleccionado.filtroCabina.codigo,
        tipo_producto: this.seleccionado.filtroCabina.tipo_producto,
        referencia: this.seleccionado.filtroCabina.referencia,
        cantidad_actual: this.cantidadProductos.filtroCabina,
        precio_unidad: this.seleccionado.filtroCabina.precio_venta,
        precio_venta: (this.cantidadProductos.filtroCabina * this.seleccionado.filtroCabina.precio_venta)
      };
      this.servicio.productos_array.push(filtroCabina);
    }
    if (precioFinalFiltroCombustible1 > 0) {
      const filtroCombustible1 = {
        _id: this.seleccionado.filtroCombustible1._id,
        filtro_combustible1: this.seleccionado.filtroCombustible1.referencia,
        codigo: this.seleccionado.filtroCombustible1.codigo,
        tipo_producto: this.seleccionado.filtroCombustible1.tipo_producto,
        referencia: this.seleccionado.filtroCombustible1.referencia,
        cantidad_actual: this.cantidadProductos.filtroCombustible1,
        precio_unidad: this.seleccionado.filtroCombustible1.precio_venta,
        precio_venta: (this.cantidadProductos.filtroCombustible1 * this.seleccionado.filtroCombustible1.precio_venta)
      };
      this.servicio.productos_array.push(filtroCombustible1);
    }
    if (precioFinalFiltroCombustible2 > 0) {
      const filtroCombustible2 = {
        _id: this.seleccionado.filtroCombustible2._id,
        filtro_combustible2: this.seleccionado.filtroCombustible2.referencia,
        codigo: this.seleccionado.filtroCombustible2.codigo,
        tipo_producto: this.seleccionado.filtroCombustible2.tipo_producto,
        referencia: this.seleccionado.filtroCombustible2.referencia,
        cantidad_actual: this.cantidadProductos.filtroCombustible2,
        precio_unidad: this.seleccionado.filtroCombustible2.precio_venta,
        precio_venta: (this.cantidadProductos.filtroCombustible2 * this.seleccionado.filtroCombustible2.precio_venta)
      };
      this.servicio.productos_array.push(filtroCombustible2);
    }
    if (precioFinalFiltroCombustible3 > 0) {
      const filtroCombustible3 = {
        _id: this.seleccionado.filtroCombustible3._id,
        filtro_combustible3: this.seleccionado.filtroCombustible3.referencia,
        codigo: this.seleccionado.filtroCombustible3.codigo,
        tipo_producto: this.seleccionado.filtroCombustible3.tipo_producto,
        referencia: this.seleccionado.filtroCombustible3.referencia,
        cantidad_actual: this.cantidadProductos.filtroCombustible3,
        precio_unidad: this.seleccionado.filtroCombustible3.precio_venta,
        precio_venta: (this.cantidadProductos.filtroCombustible3 * this.seleccionado.filtroCombustible3.precio_venta)
      };
      this.servicio.productos_array.push(filtroCombustible3);
    }
    if (precioFinalCajaValvulinas1 > 0) {
      const cajaValvulinas1 = {
        _id: this.seleccionado.cajaValvulinas1._id,
        caja1: this.seleccionado.cajaValvulinas1.referencia,
        codigo: this.seleccionado.cajaValvulinas1.codigo,
        tipo_producto: this.seleccionado.cajaValvulinas1.tipo_producto,
        referencia: this.seleccionado.cajaValvulinas1.referencia,
        cantidad_actual: this.cantidadProductos.cajaValvulinas1,
        precio_unidad: this.seleccionado.cajaValvulinas1.precio_venta,
        precio_venta: (this.cantidadProductos.cajaValvulinas1 * this.seleccionado.cajaValvulinas1.precio_venta)
      };
      this.servicio.productos_array.push(cajaValvulinas1);
    }
    if (precioFinalCajaValvulinas2 > 0) {
      const cajaValvulinas2 = {
        _id: this.seleccionado.cajaValvulinas2._id,
        caja2: this.seleccionado.cajaValvulinas2.referencia,
        codigo: this.seleccionado.cajaValvulinas2.codigo,
        tipo_producto: this.seleccionado.cajaValvulinas2.tipo_producto,
        referencia: this.seleccionado.cajaValvulinas2.referencia,
        cantidad_actual: this.cantidadProductos.cajaValvulinas2,
        precio_unidad: this.seleccionado.cajaValvulinas2.precio_venta,
        precio_venta: (this.cantidadProductos.cajaValvulinas2 * this.seleccionado.cajaValvulinas2.precio_venta)
      };
      this.servicio.productos_array.push(cajaValvulinas2);
    }
    if (precioFinalTransmisionValvulinas1 > 0) {
      const transmisionValvulinas1 = {
        _id: this.seleccionado.transmisionValvulinas1._id,
        transmision1: this.seleccionado.transmisionValvulinas1.referencia,
        codigo: this.seleccionado.transmisionValvulinas1.codigo,
        tipo_producto: this.seleccionado.transmisionValvulinas1.tipo_producto,
        referencia: this.seleccionado.transmisionValvulinas1.referencia,
        cantidad_actual: this.cantidadProductos.transmisionValvulinas1,
        precio_unidad: this.seleccionado.transmisionValvulinas1.precio_venta,
        precio_venta: (this.cantidadProductos.transmisionValvulinas1 * this.seleccionado.transmisionValvulinas1.precio_venta)
      };
      this.servicio.productos_array.push(transmisionValvulinas1);
    }
    if (precioFinalTransmisionValvulinas2 > 0) {
      const transmisionValvulinas2 = {
        _id: this.seleccionado.transmisionValvulinas2._id,
        transmision2: this.seleccionado.transmisionValvulinas2.referencia,
        codigo: this.seleccionado.transmisionValvulinas2.codigo,
        tipo_producto: this.seleccionado.transmisionValvulinas2.tipo_producto,
        referencia: this.seleccionado.transmisionValvulinas2.referencia,
        cantidad_actual: this.cantidadProductos.transmisionValvulinas2,
        precio_unidad: this.seleccionado.transmisionValvulinas2.precio_venta,
        precio_venta: (this.cantidadProductos.transmisionValvulinas2 * this.seleccionado.transmisionValvulinas2.precio_venta)
      };
      this.servicio.productos_array.push(transmisionValvulinas2);
    }
    if (precioFinalVitrina1 > 0) {
      const vitrina1 = {
        _id: this.seleccionado.vitrina1._id,
        vitrina1: this.seleccionado.vitrina1.referencia,
        codigo: this.seleccionado.vitrina1.codigo,
        tipo_producto: this.seleccionado.vitrina1.tipo_producto,
        referencia: this.seleccionado.vitrina1.referencia,
        cantidad_actual: this.cantidadProductos.vitrina1,
        precio_unidad: this.seleccionado.vitrina1.precio_venta,
        precio_venta: (this.cantidadProductos.vitrina1 * this.seleccionado.vitrina1.precio_venta)
      };
      this.servicio.productos_array.push(vitrina1);
    }
    if (precioFinalVitrina2 > 0) {
      const vitrina2 = {
        _id: this.seleccionado.vitrina2._id,
        vitrina2: this.seleccionado.vitrina2.referencia,
        codigo: this.seleccionado.vitrina2.codigo,
        tipo_producto: this.seleccionado.vitrina2.tipo_producto,
        referencia: this.seleccionado.vitrina2.referencia,
        cantidad_actual: this.cantidadProductos.vitrina2,
        precio_unidad: this.seleccionado.vitrina2.precio_venta,
        precio_venta: (this.cantidadProductos.vitrina2 * this.seleccionado.vitrina2.precio_venta)
      };
      this.servicio.productos_array.push(vitrina2);
    }
    if (precioFinalVitrina3 > 0) {
      const vitrina3 = {
        _id: this.seleccionado.vitrina3._id,
        vitrina3: this.seleccionado.vitrina3.referencia,
        codigo: this.seleccionado.vitrina3.codigo,
        tipo_producto: this.seleccionado.vitrina3.tipo_producto,
        referencia: this.seleccionado.vitrina3.referencia,
        cantidad_actual: this.cantidadProductos.vitrina3,
        precio_unidad: this.seleccionado.vitrina3.precio_venta,
        precio_venta: (this.cantidadProductos.vitrina3 * this.seleccionado.vitrina3.precio_venta)
      };
      this.servicio.productos_array.push(vitrina3);
    }
    if (precioFinalVitrina4 > 0) {
      const vitrina4 = {
        _id: this.seleccionado.vitrina4._id,
        vitrina4: this.seleccionado.vitrina4.referencia,
        codigo: this.seleccionado.vitrina4.codigo,
        tipo_producto: this.seleccionado.vitrina4.tipo_producto,
        referencia: this.seleccionado.vitrina4.referencia,
        cantidad_actual: this.cantidadProductos.vitrina4,
        precio_unidad: this.seleccionado.vitrina4.precio_venta,
        precio_venta: (this.cantidadProductos.vitrina4 * this.seleccionado.vitrina4.precio_venta)
      };
      this.servicio.productos_array.push(vitrina4);
    }
    if (precioFinalAditivo1 > 0) {
      const aditivo1 = {
        _id: this.seleccionado.aditivo1._id,
        aditivo1: this.seleccionado.aditivo1.referencia,
        codigo: this.seleccionado.aditivo1.codigo,
        tipo_producto: this.seleccionado.aditivo1.tipo_producto,
        referencia: this.seleccionado.aditivo1.referencia,
        cantidad_actual: this.cantidadProductos.aditivo1,
        precio_unidad: this.seleccionado.aditivo1.precio_venta,
        precio_venta: (this.cantidadProductos.aditivo1 * this.seleccionado.aditivo1.precio_venta)
      };
      this.servicio.productos_array.push(aditivo1);
    }
    if (precioFinalAditivo2 > 0) {
      const aditivo2 = {
        _id: this.seleccionado.aditivo2._id,
        aditivo2: this.seleccionado.aditivo2.referencia,
        codigo: this.seleccionado.aditivo2.codigo,
        tipo_producto: this.seleccionado.aditivo2.tipo_producto,
        referencia: this.seleccionado.aditivo2.referencia,
        cantidad_actual: this.cantidadProductos.aditivo2,
        precio_unidad: this.seleccionado.aditivo2.precio_venta,
        precio_venta: (this.cantidadProductos.aditivo2 * this.seleccionado.aditivo2.precio_venta)
      };
      this.servicio.productos_array.push(aditivo2);
    }
    if (precioFinalAditivo3 > 0) {
      const aditivo3 = {
        _id: this.seleccionado.aditivo3._id,
        aditivo3: this.seleccionado.aditivo3.referencia,
        codigo: this.seleccionado.aditivo3.codigo,
        tipo_producto: this.seleccionado.aditivo3.tipo_producto,
        referencia: this.seleccionado.aditivo3.referencia,
        cantidad_actual: this.cantidadProductos.aditivo3,
        precio_unidad: this.seleccionado.aditivo3.precio_venta,
        precio_venta: (this.cantidadProductos.aditivo3 * this.seleccionado.aditivo3.precio_venta)
      };
      this.servicio.productos_array.push(aditivo3);
    }
    if (precioFinalAditivo4 > 0) {
      const aditivo4 = {
        _id: this.seleccionado.aditivo4._id,
        aditivo4: this.seleccionado.aditivo4.referencia,
        codigo: this.seleccionado.aditivo4.codigo,
        tipo_producto: this.seleccionado.aditivo4.tipo_producto,
        referencia: this.seleccionado.aditivo4.referencia,
        cantidad_actual: this.cantidadProductos.aditivo4,
        precio_unidad: this.seleccionado.aditivo4.precio_venta,
        precio_venta: (this.cantidadProductos.aditivo4 * this.seleccionado.aditivo4.precio_venta)
      };
      this.servicio.productos_array.push(aditivo4);
    }
    if (precioFinalVarios1 > 0) {
      const varios1 = {
        _id: this.seleccionado.varios1._id,
        codigo: this.seleccionado.varios1.codigo,
        tipo_producto: this.seleccionado.varios1.tipo_producto,
        referencia: this.seleccionado.varios1.referencia,
        cantidad_actual: this.cantidadProductos.varios1,
        precio_unidad: this.seleccionado.varios1.precio_venta,
        precio_venta: (this.cantidadProductos.varios1 * this.seleccionado.varios1.precio_venta)
      };
      this.servicio.productos_array.push(varios1);
    }
    if (precioFinalVarios2 > 0) {
      const varios2 = {
        _id: this.seleccionado.varios2._id,
        codigo: this.seleccionado.varios2.codigo,
        tipo_producto: this.seleccionado.varios2.tipo_producto,
        referencia: this.seleccionado.varios2.referencia,
        cantidad_actual: this.cantidadProductos.varios2,
        precio_unidad: this.seleccionado.varios2.precio_venta,
        precio_venta: (this.cantidadProductos.varios2 * this.seleccionado.varios2.precio_venta)
      };
      this.servicio.productos_array.push(varios2);
    }
    if (precioFinalVarios3 > 0) {
      const varios3 = {
        _id: this.seleccionado.varios3._id,
        codigo: this.seleccionado.varios3.codigo,
        tipo_producto: this.seleccionado.varios3.tipo_producto,
        referencia: this.seleccionado.varios3.referencia,
        cantidad_actual: this.cantidadProductos.varios3,
        precio_unidad: this.seleccionado.varios3.precio_venta,
        precio_venta: (this.cantidadProductos.varios3 * this.seleccionado.varios3.precio_venta)
      };
      this.servicio.productos_array.push(varios3);
    }
    if (precioFinalVarios4 > 0) {
      const varios4 = {
        _id: this.seleccionado.varios4._id,
        codigo: this.seleccionado.varios4.codigo,
        tipo_producto: this.seleccionado.varios4.tipo_producto,
        referencia: this.seleccionado.varios4.referencia,
        cantidad_actual: this.cantidadProductos.varios4,
        precio_unidad: this.seleccionado.varios4.precio_venta,
        precio_venta: (this.cantidadProductos.varios4 * this.seleccionado.varios4.precio_venta)
      };
      this.servicio.productos_array.push(varios4);
    }
    if (precioFinalVarios5 > 0) {
      const varios5 = {
        _id: this.seleccionado.varios5._id,
        codigo: this.seleccionado.varios5.codigo,
        tipo_producto: this.seleccionado.varios5.tipo_producto,
        referencia: this.seleccionado.varios5.referencia,
        cantidad_actual: this.cantidadProductos.varios5,
        precio_unidad: this.seleccionado.varios5.precio_venta,
        precio_venta: (this.cantidadProductos.varios5 * this.seleccionado.varios5.precio_venta)
      };
      this.servicio.productos_array.push(varios5);
    }
  }

  updateProductoCantidad(id: string, producto: Producto) {
    this.productoService.getProductoById(id).subscribe(
      (data: any) => {
        const productoNuevo = {
          cantidad_actual: data.producto.cantidad_actual - producto.cantidad_actual
        };
        this.productoService.updateProducto(id, productoNuevo).subscribe(
          success => console.log('Producto actualizado correctamente'),
          error => console.log('Error al actualizar productos')
        );
      }
    );
  }

  getProductoByCodigo(codigo: string) {
    this.productoService.getProductoByCodigo(codigo).subscribe(
      (data: any) => {
        if (data.producto.codigo) {
          if (this.aceite1 === data.producto.codigo) {
            console.log('Aceite seleccionado');
            this.seleccionado.aceite1 = data.producto;
          }
          if (this.aceite2 === data.producto.codigo) {
            console.log('Aceite 2 seleccionado');
            this.seleccionado.aceite2 = data.producto;
          }
          if (this.aceite3 === data.producto.codigo) {
            console.log('Aceite 3 seleccionado');
            this.seleccionado.aceite3 = data.producto;
          }
          if (this.aceite4 === data.producto.codigo) {
            console.log('Aceite 4 seleccionado');
            this.seleccionado.aceite4 = data.producto;
          }
          if (this.filtroAceite1 === data.producto.codigo) {
            console.log('Fi aceite seleccionado');
            this.seleccionado.filtroAceite1 = data.producto;
          }
          if (this.filtroAceite2 === data.producto.codigo) {
            this.seleccionado.filtroAceite2 = data.producto;
          }
          if (this.filtroAire1 === data.producto.codigo) {
            console.log('Fi aire seleccionado');
            this.seleccionado.filtroAire1 = data.producto;
          }
          if (this.filtroAire2 === data.producto.codigo) {
            this.seleccionado.filtroAire2 = data.producto;
          }
          if (this.filtroCabina === data.producto.codigo) {
            this.seleccionado.filtroCabina = data.producto;
          }
          if (this.filtroCombustible1 === data.producto.codigo) {
            this.seleccionado.filtroCombustible1 = data.producto;
          }
          if (this.filtroCombustible2 === data.producto.codigo) {
            this.seleccionado.filtroCombustible2 = data.producto;
          }
          if (this.filtroCombustible3 === data.producto.codigo) {
            this.seleccionado.filtroCombustible3 = data.producto;
          }
          if (this.cajaValvulinas1 === data.producto.codigo) {
            this.seleccionado.cajaValvulinas1 = data.producto;
          }
          if (this.cajaValvulinas2 === data.producto.codigo) {
            this.seleccionado.cajaValvulinas2 = data.producto;
          }
          if (this.transmisionValvulinas1 === data.producto.codigo) {
            this.seleccionado.transmisionValvulinas1 = data.producto;
          }
          if (this.transmisionValvulinas2 === data.producto.codigo) {
            this.seleccionado.transmisionValvulinas2 = data.producto;
          }
          if (this.vitrina1 === data.producto.codigo) {
            this.seleccionado.vitrina1 = data.producto;
          }
          if (this.vitrina2 === data.producto.codigo) {
            this.seleccionado.vitrina2 = data.producto;
          }
          if (this.vitrina3 === data.producto.codigo) {
            this.seleccionado.vitrina3 = data.producto;
          }
          if (this.vitrina4 === data.producto.codigo) {
            this.seleccionado.vitrina4 = data.producto;
          }
          if (this.aditivo1 === data.producto.codigo) {
            this.seleccionado.aditivo1 = data.producto;
          }
          if (this.aditivo2 === data.producto.codigo) {
            this.seleccionado.aditivo2 = data.producto;
          }
          if (this.aditivo3 === data.producto.codigo) {
            this.seleccionado.aditivo3 = data.producto;
          }
          if (this.aditivo4 === data.producto.codigo) {
            this.seleccionado.aditivo4 = data.producto;
          }
          if (this.varios1 === data.producto.codigo) {
            this.seleccionado.varios1 = data.producto;
          }
          if (this.varios2 === data.producto.codigo) {
            this.seleccionado.varios2 = data.producto;
          }
          if (this.varios3 === data.producto.codigo) {
            this.seleccionado.varios3 = data.producto;
          }
          if (this.varios4 === data.producto.codigo) {
            this.seleccionado.varios4 = data.producto;
          }
          if (this.varios5 === data.producto.codigo) {
            this.seleccionado.varios5 = data.producto;
          }
        }
        console.log(this.servicio);
      },
      error => console.log('Error al traer producto por código')
    );
  }

  getClienteByCc() {
    // this.servicio.cliente_idCliente es la cedula actualmente
    this.clienteService.getClienteByCc(this.servicio.cliente_idCliente).subscribe(
      (data: any) => {
        console.log(data.cliente._id);
        this.servicio.cliente_idCliente = data.cliente._id;
        // Agrego a la db
      },
      error => console.log('Error al traer cliente por cc')
    );
  }

  calcularTotalPrecio() {
    this.servicio.total_precio_servicio
      = (this.cantidadProductos.aceite1 * this.seleccionado.aceite1.precio_venta)
      + (this.cantidadProductos.aceite2 * this.seleccionado.aceite2.precio_venta)
      + (this.cantidadProductos.aceite3 * this.seleccionado.aceite3.precio_venta)
      + (this.cantidadProductos.aceite4 * this.seleccionado.aceite4.precio_venta)
      + (this.cantidadProductos.filtroAceite1 * this.seleccionado.filtroAceite1.precio_venta)
      + (this.cantidadProductos.filtroAceite2 * this.seleccionado.filtroAceite2.precio_venta)
      + (this.cantidadProductos.filtroAire1 * this.seleccionado.filtroAire1.precio_venta)
      + (this.cantidadProductos.filtroAire2 * this.seleccionado.filtroAire2.precio_venta)
      + (this.cantidadProductos.filtroCabina * this.seleccionado.filtroCabina.precio_venta)
      + (this.cantidadProductos.filtroCombustible1 * this.seleccionado.filtroCombustible1.precio_venta)
      + (this.cantidadProductos.filtroCombustible2 * this.seleccionado.filtroCombustible2.precio_venta)
      + (this.cantidadProductos.filtroCombustible3 * this.seleccionado.filtroCombustible3.precio_venta)
      + (this.cantidadProductos.cajaValvulinas1 * this.seleccionado.cajaValvulinas1.precio_venta)
      + (this.cantidadProductos.cajaValvulinas2 * this.seleccionado.cajaValvulinas2.precio_venta)
      // tslint:disable-next-line:max-line-length
      + (this.cantidadProductos.transmisionValvulinas1 * this.seleccionado.transmisionValvulinas1.precio_venta)
      + (this.cantidadProductos.transmisionValvulinas2 * this.seleccionado.transmisionValvulinas2.precio_venta)
      + (this.cantidadProductos.vitrina1 * this.seleccionado.vitrina1.precio_venta)
      + (this.cantidadProductos.vitrina2 * this.seleccionado.vitrina2.precio_venta)
      + (this.cantidadProductos.vitrina3 * this.seleccionado.vitrina3.precio_venta)
      + (this.cantidadProductos.vitrina4 * this.seleccionado.vitrina4.precio_venta)
      + (this.cantidadProductos.aditivo1 * this.seleccionado.aditivo1.precio_venta)
      + (this.cantidadProductos.aditivo2 * this.seleccionado.aditivo2.precio_venta)
      + (this.cantidadProductos.aditivo3 * this.seleccionado.aditivo3.precio_venta)
      + (this.cantidadProductos.aditivo4 * this.seleccionado.aditivo4.precio_venta)
      + (this.cantidadProductos.varios1 * this.seleccionado.varios1.precio_venta)
      + (this.cantidadProductos.varios2 * this.seleccionado.varios2.precio_venta)
      + (this.cantidadProductos.varios3 * this.seleccionado.varios3.precio_venta)
      + (this.cantidadProductos.varios4 * this.seleccionado.varios4.precio_venta)
      + (this.cantidadProductos.varios5 * this.seleccionado.varios5.precio_venta)
      + this.servicio.valor_mano_obra_svcio;
    this.servicio.total_precio_condescuento = this.servicio.total_precio_servicio;
  }

  getParamsVehiculo() {
    this.route.params.subscribe(params => {
      if (Object.keys(params).length !== 0) {
        this.vehiculoService.getVehiculoById(params.id).subscribe(
          (data: any) => {
            this.vehiculo = data.vehiculo.placa_vh;
            this.getClienteByPlacaVehiculo(data.vehiculo.placa_vh);
          },
          error => console.log('Error al traer servicio')
        );
      }
    });
  }

  goBack() {
    this.router.navigate(['facturacion']);
  }

}
