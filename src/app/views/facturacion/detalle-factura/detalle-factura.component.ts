import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicioService } from 'src/app/services/servicio.service';
import { Servicio } from 'src/models/servicio';
import { ClienteService } from 'src/app/services/cliente.service';
import { Cliente } from 'src/models/cliente';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { Vehiculo } from 'src/models/vehiculo';
import { Logs } from 'selenium-webdriver';

@Component({
  selector: 'app-detalle-factura',
  templateUrl: './detalle-factura.component.html',
  styleUrls: ['./detalle-factura.component.scss']
})
export class DetalleFacturaComponent implements OnInit {
  servicio: Servicio;
  cliente: Cliente;
  vehiculo: Vehiculo;

  constructor(
    public route: ActivatedRoute,
    public servicioServicio: ServicioService,
    private clienteService: ClienteService,
    private vehiculoService: VehiculoService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.servicioServicio.getServicioById(params.id).subscribe(
        (data: any) => {
          this.servicio = data.servicio;
          console.log(this.servicio);
          this.getCliente();
          this.getVehiculo();
        },
        error => console.log('Error al traer servicio')
      );
    });
  }

  getCliente() {
    this.clienteService.getClienteById(this.servicio.cliente_idCliente).subscribe(
      (data: any) => this.cliente = data.cliente,
      error => console.log('Error al traer el cliente')
    );
  }

  getVehiculo() {
    this.vehiculoService.getVehiculoById(this.servicio.vehiculo_idVehiculo).subscribe(
      (data: any) => this.vehiculo = data.vehiculo,
      error => console.log('Error al traer vehiculo')
    );
  }

  imprimir(div: any) {
    const contenido = document.getElementById(div).innerHTML;
    const contenidoOriginal = document.body.innerHTML;

    document.body.innerHTML = contenido;

    window.print();

    document.body.innerHTML = contenidoOriginal;
    location.reload();
  }

}
