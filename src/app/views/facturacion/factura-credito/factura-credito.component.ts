import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { TipoPagoServicio } from 'src/models/tipoPagoServicio';
import { Credito } from 'src/models/credito';
import { ServicioCompleto } from 'src/models/servicioCompleto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-factura-credito',
  templateUrl: './factura-credito.component.html',
  styleUrls: ['./factura-credito.component.scss']
})
export class FacturaCreditoComponent implements OnInit {
  servicios: ServicioCompleto[];
  serviciosCredito: ServicioCompleto[] = [];

  servicioCredito: ServicioCompleto;

  movimientosCredito: Credito[];

  pagoCuota: number;

  filter = '';

  constructor(private servicioService: ServicioService, private router: Router) { }

  ngOnInit() {
    this.getServiciosCompleto();
  }

  getServiciosCompleto() {
    this.servicioService.getServiciosCompleto().subscribe(
      (data: any) => {
        this.servicios = data.serviciosPlacaCliente;
        console.log(this.servicios);
        console.log('dd');
        this.serviciosCredito = [];
        this.servicios.forEach(servicio => {
          // Crédito
          if (servicio.estado_pago_svcio === TipoPagoServicio.credito) {
            this.serviciosCredito.push(servicio);
            console.log('22');
          }
        });
        console.log(this.serviciosCredito);
      },
      error => console.log('Error al traer servicios')
    );
  }

  getServicioCredito(s: ServicioCompleto) {
    console.log(s);
    this.servicioCredito = s;
    // Obtener movimientos
    this.getMovimientosCredito(this.servicioCredito.servicio_idServicio);
  }

  pagarCredito() {
    const cuota = {
      servicios_idServicio: this.servicioCredito.servicio_idServicio,
      pago_cr: this.pagoCuota
    };
    console.log(cuota);
    // Pagar crédito
    this.servicioService.pagarCredito(cuota).subscribe(
      success => {
        alert('Pago realizado correctamente');
        this.getServiciosCompleto();
    },
      error => alert('Error al pagar cuota')
    );
  }

  getMovimientosCredito(id: string) {
    this.servicioService.getMovimientosCredito(id).subscribe(
      (data: any) => {
        console.log(data.credito);
        this.movimientosCredito = data.credito;
      },
      error => console.log('Error al traer los movimientos en crédito')
    );
  }

}
