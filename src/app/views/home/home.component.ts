import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { ProductoService } from 'src/app/services/producto.service';
import { ServicioService } from 'src/app/services/servicio.service';
import { Salida } from 'src/models/salida';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  cantidadClientes = 0;
  cantidadVehiculos = 0;
  cantidadProductos = 0;
  cantidadServicios = 0;

  entradasDia = 0;
  entradasMes = 0;
  salidasDia = 0;
  salidasMes = 0;

  presupuestoCreditoDia = 0;
  presupuestoCreditoMes = 0;

  presupuestoEntradasFechaDia = 0;
  presupuestoEntradasFechaMes = 0;
  presupuestoEntradasFechaAno = 0;

  presupuestoFecha = {
    day: '',
    mounth: '',
    year: ''
  };

  salida: Salida = {
    valor_sal: null,
    motivo_sal: null
  };

  checkPresupuesto = 'entradas';

  gananciasDia = 0;
  gananciasMes = 0;
  gananciasFecha: any;

  constructor(
    private clienteService: ClienteService,
    private vehiculoService: VehiculoService,
    private productoService: ProductoService,
    private servicioService: ServicioService
  ) { }

  ngOnInit() {
   this.getAll();
  }

  getCantidadClientes() {
    this.clienteService.getClientes().subscribe(
      (data: any) => this.cantidadClientes = data.clientes.length,
      error => console.log('Error al traer clientes')
    );
  }

  getCantidadVehiculos() {
    this.vehiculoService.getVehiculos().subscribe(
      (data: any) => this.cantidadVehiculos = data.vehiculos.length,
      error => console.log('Error al traer vehiculos')
    );
  }

  getCantidadProductos() {
    this.productoService.getProductos().subscribe(
      (data: any) => this.cantidadProductos = data.productos.length,
      error => console.log('Error al traer productos')
    );
  }

  getCantidadServicios() {
    this.servicioService.getServicios().subscribe(
      (data: any) => this.cantidadServicios = data.servicios.length,
      error => console.log('Error al traer servicios')
    );
  }

  getEntradasDia() {
    this.servicioService.getEntradasDia().subscribe(
      (data: any) => this.entradasDia = data.entradasDiarias,
      error => console.log('Error al traer entradas')
    );
  }
  getEntradasMes() {
    this.servicioService.getEntradasMes().subscribe(
      (data: any) => this.entradasMes = data.entradasMes,
      error => console.log('Error al traer entradas')
    );
  }

  getSalidasDia() {
    this.servicioService.getSalidasDia().subscribe(
      (data: any) => this.salidasDia = data.salidasDiarias,
      error => console.log('Error al traer salidas')
    );
  }
  getSalidasMes() {
    this.servicioService.getSalidasMes().subscribe(
      (data: any) => this.salidasMes = data.salidasMes,
      error => console.log('Error al traer salidas')
    );
  }

  getPresupuestoCreditoDia() {
    this.servicioService.getPresupuestoCreditoDia().subscribe(
      (data: any) => this.presupuestoCreditoDia = data.creditosDiarias,
      error => console.log('Error al traer presupuesto credito dia')
    );
  }

  getPresupuestoCreditoMes() {
    this.servicioService.getPresupuestoCreditoMes().subscribe(
      (data: any) => this.presupuestoCreditoMes = data.creditosMes,
      error => console.log('Error al traer presupuesto credito mes')
    );
  }

  getPresupuestoEntradasFecha() {
    const fecha = `${this.presupuestoFecha.day}${this.presupuestoFecha.mounth}${this.presupuestoFecha.year}`;
    if (
      this.presupuestoFecha.day !== '' &&
      this.presupuestoFecha.mounth !== '' &&
      this.presupuestoFecha.year !== ''
      ) {
        this.servicioService.getPresupuestoEntradasFecha(fecha).subscribe(
          (data: any) => {
            this.presupuestoEntradasFechaDia = data.entradasDia;
            this.presupuestoEntradasFechaMes = data.entradasMes;
            this.presupuestoEntradasFechaAno = data.entradasAnio;
          }
        );
    } else {
      alert('Verifica los datos');
    }
  }

  getPresupuestoSalidasFecha() {
    const fecha = `${this.presupuestoFecha.day}${this.presupuestoFecha.mounth}${this.presupuestoFecha.year}`;
    if (
      this.presupuestoFecha.day !== '' &&
      this.presupuestoFecha.mounth !== '' &&
      this.presupuestoFecha.year !== ''
      ) {
        this.servicioService.getPresupuestoSalidasFecha(fecha).subscribe(
          (data: any) => {
            this.presupuestoEntradasFechaDia = data.salidasDia;
            this.presupuestoEntradasFechaMes = data.salidasMes;
            this.presupuestoEntradasFechaAno = data.salidasAnio;
          },
          error => console.log('Verifica los datos')
        );
    } else {
      alert('Verifica los datos');
    }
  }

  getGananciasInDia() {
    this.servicioService.getGananciasInDia().subscribe(
      (data: any) => this.gananciasDia = data.gananciasTotal,
      error => console.log('Error al traer ganancias dia')
    );
  }
  getGananciasInMes() {
    this.servicioService.getGananciasInMes().subscribe(
      (data: any) => this.gananciasMes = data.gananciasTotal,
      error => console.log('Error al traer ganancias mes')
    );
  }
  getGananciasInFecha() {
    const fecha = `${this.presupuestoFecha.day}${this.presupuestoFecha.mounth}${this.presupuestoFecha.year}`;
    if (
      this.presupuestoFecha.day !== '' &&
      this.presupuestoFecha.mounth !== '' &&
      this.presupuestoFecha.year !== ''
      ) {
        this.servicioService.getGananciasInFecha(fecha).subscribe(
          (data: any) => this.gananciasFecha = data.gananciasTotal,
          error => console.log('Error al traer ganancias con la fecha: ' + fecha)
        );
    } else {
      alert('Verifica los datos');
    }
  }

  getAll() {
    this.getCantidadClientes();
    this.getCantidadVehiculos();
    this.getCantidadProductos();
    this.getCantidadServicios();
    this.getEntradasDia();
    this.getEntradasMes();
    this.getSalidasDia();
    this.getSalidasMes();
    this.getGananciasInDia();
    this.getGananciasInMes();
  }

  createSalida() {
    this.servicioService.createSalida(this.salida).subscribe(
      () => {
        alert('Salida creada correctamente');
        this.salida = {
          valor_sal: null,
          motivo_sal: null
        };
        this.getAll();
      },
      error => console.log('Error al crear salida')
    );
  }

}
