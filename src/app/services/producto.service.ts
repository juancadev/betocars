import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Producto } from 'src/models/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  url = `${environment.url}/producto`;

  constructor(private http: HttpClient) { }

  getProductos() {
    return this.http.get(this.url);
  }

  getProductoById(id: string) {
    return this.http.get(`${this.url}/${id}`);
  }

  getProductoByCodigo(codigo: string) {
    return this.http.get(`${this.url}/codigo/${codigo}`);
  }

  createProducto(producto: Producto) {
    return this.http.post(this.url, producto);
  }

  updateProducto(id: string, producto: any) {
    return this.http.put(`${this.url}/${id}`, producto);
  }

  deleteProducto(id: string) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
