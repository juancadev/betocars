import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cliente } from 'src/models/cliente';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  url = `${environment.url}/cliente`;

  constructor(private http: HttpClient) { }

  getClientes() {
    return this.http.get(this.url);
  }

  getClienteById(id: string) {
    return this.http.get(`${this.url}/${id}`);
  }

  getClienteByCc(cc: string) {
    return this.http.get(`${this.url}/cc/${cc}`);
  }

  createCliente(cliente: Cliente) {
    return this.http.post(this.url, cliente);
  }

  updateCliente(id: string, cliente: Cliente) {
    return this.http.put(`${this.url}/${id}`, cliente);
  }

  deleteCliente(id: string) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
