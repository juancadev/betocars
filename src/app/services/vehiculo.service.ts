import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Vehiculo } from 'src/models/vehiculo';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class VehiculoService {
  url = `${environment.url}/vehiculo`;

  constructor(private http: HttpClient) { }

  getVehiculos() {
    return this.http.get(this.url);
  }

  getVehiculoById(id: string) {
    return this.http.get(`${this.url}/${id}`);
  }

  getVehiculosByClienteCc(cc: string) {
    return this.http.get(`${this.url}/cccliente/${cc}`);
  }

  getClienteByPlacaVehiculo(placa: string) {
    return this.http.get(`${this.url}/placa/${placa}`);
  }

  createVehiculo(vehiculo: Vehiculo) {
    return this.http.post(this.url, vehiculo);
  }

  updateVehiculo(id: string, vehiculo: Vehiculo) {
    return this.http.put(`${this.url}/${id}`, vehiculo);
  }

  deleteVehiculo(id: string) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
