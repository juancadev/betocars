import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Servicio } from 'src/models/servicio';
import { Salida } from 'src/models/salida';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {
  url = `${environment.url}/servicio`;
  urlCredito = `${environment.url}/credito`;
  urlSalida = `${environment.url}/salida`;
  urlEntradasSalidas = `${environment.url}/presupuesto`;
  urlPresupuestoCredito = `${environment.url}/presupuestoInCredito`;
  urlGanancias = `${environment.url}/GananciasIn`;

  constructor(private http: HttpClient) { }

  getServicios() {
    return this.http.get(this.url);
  }

  getServicioById(id: string) {
    return this.http.get(`${this.url}/${id}`);
  }

  getServiciosCompleto() {
    return this.http.get(`${this.url}Completo`);
  }

  getServicioCompletoByIdCliente(id: string) {
    return this.http.get(`${this.url}CompletoByIdCliente/${id}`);
  }

  createServicio(servicio: Servicio) {
    return this.http.post(this.url, servicio);
  }

  deleteServicio(id: string) {
    return this.http.delete(`${this.url}/${id}`);
  }

  // Crédito
  getCreditoById(id: string) {
    return this.http.get(`${this.urlCredito}/${id}`);
  }

  pagarCredito(servicio: any) {
    return this.http.post(`${this.urlCredito}/pagarCuota`, servicio);
  }

  getMovimientosCredito(id: string) {
    return this.http.get(`${this.urlCredito}/idservicio/${id}`);
  }

  // Salida
  getSalidas() {
    return this.http.get(this.urlSalida);
  }

  getSalidaById(id: string) {
    return this.http.get(`${this.urlSalida}/${id}`);
  }

  createSalida(salida: Salida) {
    return this.http.post(this.urlSalida, salida);
  }

  updateSalida(id: string, salida: any) {
    return this.http.put(`${this.urlSalida}/${id}`, salida);
  }

  deleteSalida(id: string) {
    return this.http.delete(`${this.urlSalida}/${id}`);
  }

  // Entradas y salidas
  getEntradasDia() {
    return this.http.get(`${this.urlEntradasSalidas}InDia`);
  }
  getEntradasMes() {
    return this.http.get(`${this.urlEntradasSalidas}InMes`);
  }

  getSalidasDia() {
    return this.http.get(`${this.urlEntradasSalidas}OutDia`);
  }
  getSalidasMes() {
    return this.http.get(`${this.urlEntradasSalidas}OutMes`);
  }

  // Presupuesto Creditos

  getPresupuestoCreditoDia() {
    return this.http.get(`${this.urlPresupuestoCredito}Dia`);
  }
  getPresupuestoCreditoMes() {
    return this.http.get(`${this.urlPresupuestoCredito}Mes`);
  }

  // Presupuesto por fecha
  getPresupuestoEntradasFecha(fecha: string) {
    return this.http.get(`${this.urlEntradasSalidas}EntradasFecha/${fecha}`);
  }
  getPresupuestoSalidasFecha(fecha: string) {
    return this.http.get(`${this.urlEntradasSalidas}SalidasFecha/${fecha}`);
  }

  // Ganancias
  getGananciasInDia() {
    return this.http.get(`${this.urlGanancias}Dia`);
  }
  getGananciasInMes() {
    return this.http.get(`${this.urlGanancias}Mes`);
  }
  getGananciasInFecha(fecha: string) {
    return this.http.get(`${this.urlGanancias}Fecha/${fecha}`);
  }
}
