import { TipoVehiculo } from './tipoVehiculo';

export interface Vehiculo {
  _id?: string;
  tipoVehiculos: TipoVehiculo;
  cliente_idCliente: string;
  placa_vh: string;
  marca_vh?: string;
  modelo_vh?: string;
  aceite?: string;
  filtro_aceite?: string;
  filtro_aire?: string;
  filtro_cabina?: string;
  filtro_combustible?: string;
  caja?: string;
  transmision?: string;
  aditivo?: string;
  vitrina?: string;
  otros_vh?: string;
}
