export interface Credito {
  _id?: string;
  fecha_ultimo_pago_cr: string;
  total_cr: number;
  saldo_cr: number;
  pago_cr: number;
  servcios_idServicio: string;
}
