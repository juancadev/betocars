export interface Salida {
  _id?: string;
  valor_sal: number;
  motivo_sal: string;
}
