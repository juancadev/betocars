export interface Cliente {
  _id?: string;
  nombres_cliente: string;
  apellidos_cliente?: string;
  cc_cliente: string;
  cel_cliente?: string;
  correo_cliente?: string;
  direccion_cliente?: string;
}
