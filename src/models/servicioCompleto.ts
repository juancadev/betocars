import { TipoPagoServicio } from './tipoPagoServicio';

export interface ServicioCompleto {
  _id?: string;
  servicio_idServicio: string;
  nro_factura_svcio: number;
  estado_pago_svcio: TipoPagoServicio;
  placa_vh?: string;
  nombres_cliente?: string;
  apellidos_cliente?: string;
  cc_cliente?: string;
  total_precio_condescuento: number;
  valor_cancelado_svcio: number;
  fecha_inicial_svcio: string;
}
