export enum TipoProducto {
  'Aceite',
  'Filtro de aceite',
  'Filtro de aire',
  'Filtro de cabina',
  'Filtro de combustible',
  'Vitrina',
  'Aditivo'
}
