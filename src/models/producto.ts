import { TipoProducto } from './tipoProducto';

export interface Producto {
  _id?: string;
  tipo_producto: TipoProducto;
  codigo: string;
  referencia: string;
  cantidad_actual: number;
  precio_compra: number;
  precio_venta: number;
  notas: string;
  tipo_vh_compatible: string;
  equivalencia: string;
}
