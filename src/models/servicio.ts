import { TipoPagoServicio } from './tipoPagoServicio';

export interface Servicio {
  _id?: string;
  nro_factura_svcio: number;
  vehiculo_idVehiculo?: string;
  placa_vehiculo?: string;
  cliente_idCliente: string;
  nombre_cliente?: string;
  // Productos
  productos_array?: any;
  aceite_km_actual?: string;
  aceite_km_proximo?: string;
  caja_km_actual?: string;
  caja_km_proximo?: string;
  transmision_km_actual?: string;
  transmision_km_proximo?: string;
  descripcion_mano_obra_svcio?: string;
  valor_mano_obra_svcio: number;
  estado_pago_svcio: TipoPagoServicio;

  total_precio_servicio: number;
  total_precio_condescuento: number;
  valor_cancelado_svcio: number;

  otros_svcio?: string;
}
